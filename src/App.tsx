import React, { useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import { Row, Card, Table, Col, Alert } from "react-bootstrap";
import "./App.css";
import { useRef } from "react";
interface userType {
  id: number;
  price: number;
  title: string;
}

const userData = [
  {
    id: 1,
    price: 20,
    title: "mango",
  },
  {
    id: 2,
    price: 10,
    title: "jango",
  },
] as any[];
interface UserNewType extends userType {
  count: number;
  active: boolean;
  total: number;
}
function App() {
  const [data, setData] = React.useState(userData);
  const [selectData, setSelectData] = React.useState<any[]>([]);
  const [count, setCount] = React.useState(0);
  const increment = (id) => {
    setCount(count + 1);
    const copyUserData = [...data];
    const index = copyUserData.findIndex((i) => id === i.id);
    let customDatas: any = copyUserData[index];
    customDatas.count = customDatas.count + 1;
    customDatas.total = customDatas.count * customDatas.price;
    setData(copyUserData);
    setSelectData(() => [...selectDataDublicateCheck, customDatas]);
    totalCac();
  };
  const handleCardReq = (id) => {
    const copyUserData = [...data];
    const index = copyUserData.findIndex((i) => id === i.id);
    let customDatas: any = copyUserData[index];
    customDatas.active = true;
    customDatas.count = 0;
    increment(id);
  };

  const decrement = (id) => {
    const copyUserData = [...data];
    const index = copyUserData.findIndex((i) => id === i.id);
    let customDatas = copyUserData[index];
    if (customDatas.count > 0) customDatas.count = customDatas.count - 1;
    customDatas.total = customDatas.count * customDatas.price;
    setSelectData(() => [...selectDataDublicateCheck, customDatas]);

    setCount(count - 1);

    // if (count > 0) {
    // } else {
    //   copyUserData[index].active = false;
    //   copyUserData[index].count = 0;
    //   setCount(0);
    //   setData(data);
    //   setSelectData([...selectDataDublicateCheck, customDatas]);
    // }
    totalCac();
  };

  const uniqueIds: any[] = [];
  const selectDataDublicateCheck = selectData.filter((element) => {
    const isDuplicate = uniqueIds.includes(element.id);
    if (!isDuplicate) {
      uniqueIds.push(element.id);
      return true;
    }
    return false;
  });
  const totalCac = () => {
    let total = selectDataDublicateCheck.reduce(function (
      previousValue,
      currentValue
    ) {
      return previousValue + currentValue.total;
    },
    0);
    return total;
  };

  return (
    <div>
      <div className="text-center">
        <Alert variant={"primary"}>
          <pre> {JSON.stringify(data, null)}</pre>
          {JSON.stringify(count)}
        </Alert>
      </div>
      <Container>
        <Row>
          {data &&
            data.map((i, k) => {
              console.log(i.active > 0);
              return (
                <Col key={k} xs={4}>
                  <Card>
                    <Card.Img
                      variant="top"
                      src="https://assets.st-note.com/production/uploads/images/80584560/rectangle_large_type_2_177d002b8483e64ec74ac91e7d343b9b.png?width=800"
                    />
                    <Card.Body>
                      <Card.Title>{i.title}</Card.Title>
                      <Card.Text>Price {i.price}</Card.Text>
                      {i.active > 0 && i.total > 0 ? (
                        <span>
                          <Button
                            variant="primary"
                            onClick={() => increment(i.id)}
                          >
                            +
                          </Button>
                          <span className="txt-ce">{i.count}</span>
                          <Button
                            variant="primary"
                            onClick={() => decrement(i.id)}
                          >
                            -
                          </Button>
                        </span>
                      ) : (
                        <span>
                          <Button
                            variant="primary"
                            onClick={() => handleCardReq(i.id)}
                          >
                            Buy now
                          </Button>
                        </span>
                      )}
                    </Card.Body>
                  </Card>
                </Col>
              );
            })}
        </Row>
        <Row>
          {count > 0 && (
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>id</th>
                  <th>Product Name</th>
                  <th>Product price</th>
                  <th>Product Quantity</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>
                {selectDataDublicateCheck &&
                  selectDataDublicateCheck.map((i, k) => {
                    if (i.count > 0)
                      return (
                        <tr key={k}>
                          <td>{i.id}</td>
                          <td>{i.title}</td>
                          <td>{i.price}</td>
                          <td>{i.count}</td>
                          <td>{i.total}</td>
                        </tr>
                      );
                  })}
              </tbody>
            </Table>
          )}

          <Alert variant={"primary"}>TOTAL ${totalCac()}</Alert>
        </Row>
      </Container>
    </div>
  );
}

export default App;
